# Ansible Collection - c2platform.azure_devops

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-azure_devops/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-azure_devops/-/pipelines) [![Latest Release](https://gitlab.com/c2platform/ansible-collection-azure_devops/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-azure_devops/-/pipelines)

See full [README](https://gitlab.com/c2platform/ansible-collection-azure_devops/-/blob/master/README.md).