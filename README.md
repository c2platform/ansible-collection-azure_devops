# Ansible Collection - c2platform.azure_devops

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-azure_devops/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-azure_devops/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-azure_devops/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-azure_devops/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.gis-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/azure_devops/)

## Roles

* [build_agent](./roles/build_agent) create build or deployment agent

## Modules and Filters

For detailed information on the available modules and filters within this
collection, please refer to the
[Ansible Galaxy](https://galaxy.ansible.com/ui/repo/published/c2platform/azure_devops/docs/)
page.

You can of course also use the `ansible-doc` command to explore the
documentation:

```bash
ansible-doc -t module --list c2platform.azure_devops
ansible-doc -t filter --list c2platform.azure_devops
ansible-doc -t filter <FILTER_NAME>
ansible-doc -t module <MODULE_NAME>
```
